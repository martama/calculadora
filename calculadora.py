'''
Crea un programa que haga funciones de calculadora simple. El programa ten-
drá dos funciones (sumar, restar). Cada función aceptará dos parámetros, y devol-
verá su suma o su diferencia. También tendrá un programa principal que llamará
a esas funciones para sumar primero 1 y 2, y luego 3 y 4, mostrando en cada caso
el resultado en pantalla. A continuación restará primero 5 de 6, y luego 7 de 8,
mostrando también los resultados en pantalla
'''

def sumar(n1, n2):
    result = n1 + n2
    return result

def restar(n1, n2):
    result = n1 - n2
    return result

sum1 = sumar(1, 2)
print(f"1 + 2 = {sum1}")

sum2 = sumar(3, 4)
print(f"3 + 4 = {sum2}")

res1 = restar(6, 5)
print(f"6 - 5 = {res1}")

res2 = restar(8, 7)
print(f"8 - 7 = {res2}")